#!/usr/bin/env bash

set -eux -o pipefail

projdir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

cd "$projdir"
if [[ -f .env ]]; then
    source .env
fi

SONAR_TOKEN="$SONAR_TOKEN" papermill notebook.ipynb /dev/null
